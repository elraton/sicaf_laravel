<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::prefix('v1')->group(function(){
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('getUser', 'AuthController@getUser');

        // Personal Category
        Route::get('personal/category/list', 'PersonalCategoryController@index');
        Route::post('personal/category/add', 'PersonalCategoryController@store');
        Route::post('personal/category/edit', 'PersonalCategoryController@update');
        Route::post('personal/category/remove', 'PersonalCategoryController@delete');

        // Personal Schedule
        Route::get('personal/schedule/list', 'PersonalScheduleController@index');
        Route::post('personal/schedule/add', 'PersonalScheduleController@store');
        Route::post('personal/schedule/edit', 'PersonalScheduleController@update');
        Route::post('personal/schedule/remove', 'PersonalScheduleController@delete');

        // Personal
        Route::get('personal/list', 'PersonalController@index');
        Route::post('personal/add', 'PersonalController@store');
        Route::post('personal/edit', 'PersonalController@update');
        Route::post('personal/remove', 'PersonalController@delete');
        Route::post('personal/get', 'PersonalController@show');

        //IO Personal
        Route::get('iopersonal/list', 'IOPersonalController@index');
        Route::post('iopersonal/add', 'IOPersonalController@store');
        Route::post('iopersonal/day', 'IOPersonalController@byDay');
        Route::post('iopersonal/turn', 'IOPersonalController@byTurn');

        // Tarifas Servicios
        Route::get('tarifas/list', 'TarifasServiciosController@index');
        Route::post('tarifas/add', 'TarifasServiciosController@store');
        Route::post('tarifas/edit', 'TarifasServiciosController@update');
        Route::post('tarifas/remove', 'TarifasServiciosController@delete');

        // Codigos Promociones
        Route::get('promociones/list', 'CodigosPromosController@index');
        Route::post('promociones/add', 'CodigosPromosController@store');
        Route::post('promociones/edit', 'CodigosPromosController@update');
        Route::post('promociones/remove', 'CodigosPromosController@delete');

        // Ventas Caja
        Route::get('ventacaja/list', 'VentasCajaController@index');
        Route::post('ventacaja/add', 'VentasCajaController@store');
        Route::post('ventacaja/edit', 'VentasCajaController@update');
        Route::post('ventacaja/remove', 'VentasCajaController@delete');

        // Pacientes
        Route::get('paciente/list', 'PacientesController@index');
        Route::post('paciente/search', 'PacientesController@searchbyDNI');
        Route::post('paciente/add', 'PacientesController@store');
        Route::post('paciente/edit', 'PacientesController@update');
        Route::post('paciente/remove', 'PacientesController@delete');

        // Atencion Diaria
        Route::get('atenciondiaria/list', 'AtencionDiariaController@index');
        Route::post('atenciondiaria/add', 'AtencionDiariaController@store');
        Route::post('atenciondiaria/edit', 'AtencionDiariaController@update');
        Route::post('atenciondiaria/remove', 'AtencionDiariaController@delete');

        // Ingreso Caja
        Route::get('ingresocaja/list', 'IngresoCajaController@index');
        Route::post('ingresocaja/add', 'IngresoCajaController@store');
        Route::post('ingresocaja/edit', 'IngresoCajaController@update');
        Route::post('ingresocaja/remove', 'IngresoCajaController@delete');

        // Egreso Caja
        Route::get('egresocaja/list', 'EgresoCajaController@index');
        Route::post('egresocaja/add', 'EgresoCajaController@store');
        Route::post('egresocaja/edit', 'EgresoCajaController@update');
        Route::post('egresocaja/remove', 'EgresoCajaController@delete');

        // Servicio Paciente
        Route::get('servpaciente/list', 'ServicioPacienteController@index');
        Route::post('servpaciente/get', 'ServicioPacienteController@getOne');
        Route::post('servpaciente/add', 'ServicioPacienteController@store');
        Route::post('servpaciente/edit', 'ServicioPacienteController@update');
        Route::post('servpaciente/remove', 'ServicioPacienteController@delete');
        Route::post('servpaciente/range', 'ServicioPacienteController@getbyRange');

        // Horarios paciente
        Route::post('horarios/range', 'HorarioController@getbyrange');
        Route::post('horarios/edit', 'HorarioController@update');
        Route::post('horarios/saveOne', 'HorarioController@saveOne');
    });
    
});