<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Roles;
use App\Tarifas_Servicios;
use App\Personal;
use App\Paciente;
use App\Per_Horario;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->NOMBRE = 'Mayra';
        $user->APELLIDO = 'Lizarve';
        $user->email = 'mayra@fisiovida.com';
        $user->username = 'mayra';
        $user->password = bcrypt('mayra');
        $user->AREA = 'recepcion';
        $user->SEDE = 'RONDA RECOLETA';
        $user->TURNO = 'TARDE';
        $user->photo = 'female_user.jpg';
        $user->save();

        $user = new User();
        $user->NOMBRE = 'Jonathan';
        $user->APELLIDO = 'Flores';
        $user->email = 'jonathan.flores@fisiovida.com';
        $user->username = 'panda';
        $user->password = bcrypt('panda');
        $user->AREA = 'administracion';
        $user->SEDE = 'RONDA RECOLETA';
        $user->TURNO = 'TARDE';
        $user->photo = 'female_user.jpg';
        $user->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 100;
        $tarifa->SERVICIO = 'Fisiatra';
        $tarifa->TIPO_TARIFA = 'FULLPRICE';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 100.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 101;
        $tarifa->SERVICIO = 'Fisiatra';
        $tarifa->TIPO_TARIFA = 'PROMOCIONAL';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 90.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 102;
        $tarifa->SERVICIO = 'Fisiatra';
        $tarifa->TIPO_TARIFA = 'ESPECIAL';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 80.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 103;
        $tarifa->SERVICIO = 'Fisiatra';
        $tarifa->TIPO_TARIFA = 'FAMILIAR';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 80.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 104;
        $tarifa->SERVICIO = 'Fisiatra';
        $tarifa->TIPO_TARIFA = 'FULLPRICE';
        $tarifa->TIPO_PACIENTE = 'NIÑO';
        $tarifa->PRECIO = 100.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 105;
        $tarifa->SERVICIO = 'Fisiatra';
        $tarifa->TIPO_TARIFA = 'PROMOCIONAL';
        $tarifa->TIPO_PACIENTE = 'NIÑO';
        $tarifa->PRECIO = 90.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 106;
        $tarifa->SERVICIO = 'Fisiatra';
        $tarifa->TIPO_TARIFA = 'ESPECIAL';
        $tarifa->TIPO_PACIENTE = 'NIÑO';
        $tarifa->PRECIO = 80.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 107;
        $tarifa->SERVICIO = 'Fisiatra';
        $tarifa->TIPO_TARIFA = 'FAMILIAR';
        $tarifa->TIPO_PACIENTE = 'NIÑO';
        $tarifa->PRECIO = 80.00;
        $tarifa->save();

        /** Traumatologia */

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 108;
        $tarifa->SERVICIO = 'Traumatologia';
        $tarifa->TIPO_TARIFA = 'FULLPRICE';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 100.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 109;
        $tarifa->SERVICIO = 'Traumatologia';
        $tarifa->TIPO_TARIFA = 'PROMOCIONAL';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 90.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 110;
        $tarifa->SERVICIO = 'Traumatologia';
        $tarifa->TIPO_TARIFA = 'ESPECIAL';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 80.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 111;
        $tarifa->SERVICIO = 'Traumatologia';
        $tarifa->TIPO_TARIFA = 'FAMILIAR';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 80.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 112;
        $tarifa->SERVICIO = 'Traumatologia';
        $tarifa->TIPO_TARIFA = 'FULLPRICE';
        $tarifa->TIPO_PACIENTE = 'NIÑO';
        $tarifa->PRECIO = 100.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 113;
        $tarifa->SERVICIO = 'Traumatologia';
        $tarifa->TIPO_TARIFA = 'PROMOCIONAL';
        $tarifa->TIPO_PACIENTE = 'NIÑO';
        $tarifa->PRECIO = 90.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 114;
        $tarifa->SERVICIO = 'Traumatologia';
        $tarifa->TIPO_TARIFA = 'ESPECIAL';
        $tarifa->TIPO_PACIENTE = 'NIÑO';
        $tarifa->PRECIO = 80.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 115;
        $tarifa->SERVICIO = 'Traumatologia';
        $tarifa->TIPO_TARIFA = 'FAMILIAR';
        $tarifa->TIPO_PACIENTE = 'NIÑO';
        $tarifa->PRECIO = 80.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 116;
        $tarifa->SERVICIO = 'Fisioterapia';
        $tarifa->TIPO_TARIFA = 'FULLPRICE';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 40.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 117;
        $tarifa->SERVICIO = 'Fisioterapia';
        $tarifa->TIPO_TARIFA = 'PROMOCIONAL';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 30.00;
        $tarifa->save();

        $tarifa = new Tarifas_Servicios();
        $tarifa->CODIGO = 118;
        $tarifa->SERVICIO = 'Fisioterapia';
        $tarifa->TIPO_TARIFA = 'ESPECIAL';
        $tarifa->TIPO_PACIENTE = 'ADULTO';
        $tarifa->PRECIO = 20.00;
        $tarifa->save();

        /** personal */

        $personal = new Personal;
        $personal->DNI = "46590597";
        $personal->NOMBRE = "Jhosimar";
        $personal->NOMBRE2 = "";
        $personal->APELLIDO = "Mamani";
        $personal->APELLIDO2 = "Amanqui";
        $personal->CELULAR = "974359185";
        $personal->TELEFONO = "";
        $personal->SEDE = "RONDA RECOLETA";
        $personal->TURNO = "TARDE";
        $personal->FECHA_INGRESO = '2019-05-10';
        $personal->AREA = "FISIOTERAPEUTA";
        $personal->FECHA_NACIMIENTO = "1990-09-11";
        $personal->DEPARTAMENTO = "2900";
        $personal->CIUDAD = "2901";
        $personal->DIRECCION = "DIreccion";
        $personal->DISTRITO = "2911";
        $personal->ESTADO = "ACTIVO";
        $personal->CONDICION = "CONTRATO";
        $personal->EMAIL = "email@email.com";
        $personal->ULTIMO_TRABAJO = "";
        $personal->ULTIMA_POSICION = "";
        $personal->ESTADO_CIVIL = "SOLTERO";
        $personal->NMRO_CUENTA = "";
        $personal->ES_DOCTOR = 1;
        $personal->ES_NIÑOS = 0;
        $personal->PASSWORD = "1234";
        $personal->save();

        $personal = new Personal;
        $personal->DNI = "46590598";
        $personal->NOMBRE = "Fisioterapeuta";
        $personal->NOMBRE2 = "";
        $personal->APELLIDO = "XXX";
        $personal->APELLIDO2 = "YYY";
        $personal->CELULAR = "974359185";
        $personal->TELEFONO = "";
        $personal->SEDE = "RONDA RECOLETA";
        $personal->TURNO = "TARDE";
        $personal->FECHA_INGRESO = '2019-05-10';
        $personal->AREA = "FISIOTERAPEUTA";
        $personal->FECHA_NACIMIENTO = "1990-09-11";
        $personal->DEPARTAMENTO = "2900";
        $personal->CIUDAD = "2901";
        $personal->DIRECCION = "DIreccion";
        $personal->DISTRITO = "2911";
        $personal->ESTADO = "ACTIVO";
        $personal->CONDICION = "CONTRATO";
        $personal->EMAIL = "email@email.com";
        $personal->ULTIMO_TRABAJO = "";
        $personal->ULTIMA_POSICION = "";
        $personal->ESTADO_CIVIL = "SOLTERO";
        $personal->NMRO_CUENTA = "";
        $personal->ES_DOCTOR = 0;
        $personal->ES_NIÑOS = 0;
        $personal->PASSWORD = "1234";
        $personal->save();


        /** Pactiente */
        $paciente = new Paciente;
        $paciente->SEDE = "RONDA RECOLETA";
        $paciente->DNI = "46590597";
        $paciente->P_NOMBRE = "Jhosimar";
        $paciente->S_NOMBRE = "";
        $paciente->P_APELLIDO = "Mamani";
        $paciente->S_APELLIDO = "Amanqui";
        $paciente->CELULAR = "974359185";
        $paciente->TELEFONO = "";
        $paciente->DIRECCION = "Direccion 2";
        $paciente->DISTRITO = "2911";
        $paciente->F_NACIMIENTO = "1990-09-11";
        $paciente->NOMBRE_TUTOR = "tutor";
        $paciente->CELULAR_TUTOR = "123123123";
        $paciente->EMAIL = "email@email.com";
        $paciente->TIPO_PACIENTE = "SEGURO";
        $paciente->ASEGURADORA = "POSITIVA";
        $paciente->TIPO_SEGURO = "SOAT";
        $paciente->COPAGO_FIJO = 90.00;
        $paciente->COPAGO_VARIABLE = 30.00;
        $paciente->F_REGISTRO = "2019-05-14";
        $paciente->HHCC = 1;
        $paciente->save();

        /** Horario */
        $horario = new Per_Horario;
        $horario->DIA = 'ma';
        $horario->HORA_INICIO = '15:00:00';
        $horario->HORA_FIN = '17:00:00';
        $horario->NOMBRE = 'Martes Doctor';
        $horario->TIEMPO_ATENCION = 15;
        $horario->save();

        $horario = new Per_Horario;
        $horario->DIA = 'ju';
        $horario->HORA_INICIO = '15:00:00';
        $horario->HORA_FIN = '17:00:00';
        $horario->NOMBRE = 'Jueves Doctor';
        $horario->TIEMPO_ATENCION = 15;
        $horario->save();

        /** Horario personal */
        $horario = new Per_Horario;
        $horario->DIA = 'lu';
        $horario->HORA_INICIO = '07:00:00';
        $horario->HORA_FIN = '13:00:00';
        $horario->NOMBRE = 'Lunes adultos mañana';
        $horario->TIEMPO_ATENCION = 60;
        $horario->save();
        
        $horario = new Per_Horario;
        $horario->DIA = 'ma';
        $horario->HORA_INICIO = '07:00:00';
        $horario->HORA_FIN = '13:00:00';
        $horario->NOMBRE = 'Martes adultos mañana';
        $horario->TIEMPO_ATENCION = 60;
        $horario->save();

        $horario = new Per_Horario;
        $horario->DIA = 'mi';
        $horario->HORA_INICIO = '07:00:00';
        $horario->HORA_FIN = '13:00:00';
        $horario->NOMBRE = 'Miercoles adultos mañana';
        $horario->TIEMPO_ATENCION = 60;
        $horario->save();

        $horario = new Per_Horario;
        $horario->DIA = 'ju';
        $horario->HORA_INICIO = '07:00:00';
        $horario->HORA_FIN = '13:00:00';
        $horario->NOMBRE = 'Jueves adultos mañana';
        $horario->TIEMPO_ATENCION = 60;
        $horario->save();

        $horario = new Per_Horario;
        $horario->DIA = 'vi';
        $horario->HORA_INICIO = '07:00:00';
        $horario->HORA_FIN = '13:00:00';
        $horario->NOMBRE = 'Viernes adultos mañana';
        $horario->TIEMPO_ATENCION = 60;
        $horario->save();

        $horario = new Per_Horario;
        $horario->DIA = 'sa';
        $horario->HORA_INICIO = '07:00:00';
        $horario->HORA_FIN = '15:00:00';
        $horario->NOMBRE = 'Sabado adultos';
        $horario->TIEMPO_ATENCION = 60;
        $horario->save();
    }
}
