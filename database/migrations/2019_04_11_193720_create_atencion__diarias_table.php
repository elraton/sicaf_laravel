<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtencionDiariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atencion__diarias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('FECHA');
            $table->string('TERAPEUTA', 50);
            $table->string('NOMBRE_APELLIDO', 50);
            $table->string('ASEGURADORA', 25);
            $table->string('TIPO_SEGURO', 25);
            $table->string('TERAPIAS', 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atencion__diarias');
    }
}
