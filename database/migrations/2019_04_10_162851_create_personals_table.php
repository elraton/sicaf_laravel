<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('DNI', 8)->unique();
            $table->string('NOMBRE', 30);
            $table->string('NOMBRE2', 30)->nullable();
            $table->string('APELLIDO', 30);
            $table->string('APELLIDO2', 30);
            $table->string('CELULAR', 10);
            $table->string('TELEFONO', 10)->nullable();
            $table->string('SEDE', 20);
            $table->string('TURNO', 6);
            $table->string('AREA', 14);
            $table->date('FECHA_INGRESO')->nullable();
            $table->date('FECHA_NACIMIENTO');
            $table->string('DEPARTAMENTO', 45);
            $table->string('CIUDAD', 45);
            $table->string('DIRECCION', 100);
            $table->string('DISTRITO', 45);
            $table->string('ESTADO', 8);
            $table->string('CONDICION', 8)->nullable();
            $table->string('EMAIL', 50)->nullable();
            
            $table->string('PASSWORD');

            $table->string('ULTIMO_TRABAJO', 50)->nullable();
            $table->string('ULTIMA_POSICION', 50)->nullable();
            $table->string('ESTADO_CIVIL', 50);
            $table->string('NMRO_CUENTA', 20)->nullable();
            $table->boolean('ES_DOCTOR');
            $table->boolean('ES_NIÑOS');
            $table->integer('CATEGORIA')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personals');
    }
}
