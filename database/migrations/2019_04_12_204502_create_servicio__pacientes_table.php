<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicioPacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio__pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('DNI', 8);
            $table->string('P_NOMBRE', 20);
            $table->string('P_APELLIDO', 25);
            $table->string('TERAPEUTA', 25)->nullable();
            $table->integer('TERAPEUTA_ID')->nullable();
            $table->string('DOCTOR', 25)->nullable();
            $table->integer('DOCTOR_ID')->nullable();
            $table->string('ESTADO', 8);
            $table->enum('INTERVALO', ['lu-vi', 'lu-sa', 'lu-mi-vi', 'ma-ju-sa'])->nullable();
            $table->date('FECHA');
            $table->time('HORARIO');
            $table->time('HORARIO2')->nullable();
            $table->integer('S_PROGRAMADAS');
            $table->integer('S_REALIZADAS');
            $table->integer('S_PAGADAS');
            $table->integer('CONSULTA_GRATIS')->nullable();
            $table->integer('SERVICIO')->nullable();
            $table->integer('ABONO_TERAPIA')->nullable();
            $table->integer('T_PROMOCION')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio__pacientes');
    }
}
