<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorariosServicioPaciente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios_servicio__paciente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('horarios_id');
            $table->unsignedBigInteger('servicio__paciente_id');
            $table->timestamps();

            /*$table->foreign('horarios_id')->references('id')->on('horarios')->onDelete('cascade');
            $table->foreign('servicio__paciente_id')->references('id')->on('servicio__paciente')->onDelete('cascade');

            $table->primary(['horarios_id', 'servicio__paciente_id']);*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios_servicio__paciente');
    }
}
