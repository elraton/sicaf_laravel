<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngresosCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingresos__cajas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('FECHA');
            $table->string('DNI', 8);
            $table->string('NOMBRE_APELLIDO', 50);
            $table->string('TIPO_PACIENTE', 20);
            $table->string('TIPO_SERVICIO', 50);
            $table->decimal('PRECIO_UNIDAD', 8, 2);
            $table->integer('CANTIDAD');
            $table->decimal('SUBTOTAL', 8, 2);
            $table->decimal('IGV', 8, 2);
            $table->decimal('TOTAL', 8, 2);
            $table->decimal('TOTAL_DESCUENTO', 8, 2);
            $table->integer('DESCUENTO');
            $table->decimal('TOTAL_PAGADO', 8, 2);
            $table->string('PROMO_MENSUAL', 40);
            $table->string('COD_PROMOCION', 6);
            $table->string('TIPO_CODIGO', 2);
            $table->string('FORMA_PAGO', 8);
            $table->string('TIPO_COMPROBANTE', 40);
            $table->string('RECEPTOR', 35);
            $table->string('TURNO', 6);
            $table->string('SEDE', 20);                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingresos__cajas');
    }
}
