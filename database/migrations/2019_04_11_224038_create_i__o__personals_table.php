<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIOPersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('i__o__personals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('FECHA');
            $table->string('CODIGO');
            $table->string('NOMBRE_APELLIDO', 35);
            $table->string('TIPO_REGISTRO', 10);
            $table->string('TURNO', 6);
            $table->string('SEDE', 20);
            $table->time('HORA_PERSONAL');
            $table->time('HORA_REGISTRO');
            $table->string('ESTADO', 12);
            $table->integer('ATENCIONES');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('i__o__personals');
    }
}
