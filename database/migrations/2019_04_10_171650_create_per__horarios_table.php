<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per__horarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('DIA', ['lu', 'ma', 'mi', 'ju', 'vi', 'sa']);
            $table->time('HORA_INICIO');
            $table->time('HORA_FIN');
            $table->string('NOMBRE', 50);
            $table->integer('TIEMPO_ATENCION'); // en minutos 15 - 30 - 60
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('per__horarios');
    }
}
