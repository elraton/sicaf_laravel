<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEgresosCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('egresos__cajas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('FECHA');
            $table->string('SEDE', 30);
            $table->string('EMISOR', 35);
            $table->string('RECEPTOR', 35);
            $table->decimal('IMPORTE', 8, 2);
            $table->string('TURNO', 10);
            $table->string('DESCRIPCION', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('egresos__cajas');
    }
}
