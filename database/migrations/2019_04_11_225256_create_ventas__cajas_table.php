<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas__cajas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('FECHA');
            $table->string('ITEM', 30);
            $table->string('MARCA', 30);
            $table->string('TIPO', 30);
            $table->string('MODELO', 30);
            $table->string('CANTIDAD', 30);
            $table->decimal('PRECIO_UNIDAD', 8, 2);
            $table->decimal('SUBTOTAL', 8, 2);
            $table->decimal('IGV', 8, 2);
            $table->decimal('TOTAL', 8, 2);
            $table->string('PERSONAL_FISIOVIDA', 40);
            $table->integer('CODIGO_VENTA');
            $table->string('SEDE', 20);
            $table->string('USUARIO', 35);
            $table->string('TURNO', 6);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas__cajas');
    }
}
