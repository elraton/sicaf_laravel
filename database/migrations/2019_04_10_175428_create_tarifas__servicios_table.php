<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTarifasServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarifas__servicios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('CODIGO');
            $table->string('SERVICIO', 50);
            $table->enum('TIPO_TARIFA', ['fullprice', 'promocional', 'especial', 'familiar']);
            $table->enum('TIPO_PACIENTE', ['niño', 'adulto']); 
            $table->decimal('PRECIO', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarifas__servicios');
    }
}
