<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('HHCC');
            $table->string('SEDE', 20);
            $table->string('DNI', 8);
            $table->string('P_NOMBRE', 15);
            $table->string('S_NOMBRE', 15)->nullable();
            $table->string('P_APELLIDO', 20);
            $table->string('S_APELLIDO', 20);
            $table->string('CELULAR', 9);
            $table->string('TELEFONO', 9)->nullable();
            $table->string('DIRECCION', 100);
            $table->string('DISTRITO', 50);
            $table->date('F_NACIMIENTO');
            $table->string('NOMBRE_TUTOR', 25);
            $table->string('CELULAR_TUTOR', 25);
            $table->string('EMAIL', 50);
            $table->string('TIPO_PACIENTE', 10);
            $table->string('ASEGURADORA', 10);
            $table->string('TIPO_SEGURO', 15);
            $table->decimal('COPAGO_FIJO', 5, 2)->nullable();
            $table->decimal('COPAGO_VARIABLE', 5, 2)->nullable();
            $table->date('F_REGISTRO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
