<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Atencion_Diaria;

class AtencionDiariaController extends Controller
{
    public function index() {
        $atenciones = Atencion_Diaria::all();

        return $atenciones->toJson();
    }

    public function store(Request $request) {
        $atencion = new Atencion_Diaria;
        $atencion->FECHA = $request->FECHA;
        $atencion->TERAPEUTA = $request->TERAPEUTA;
        $atencion->NOMBRE_APELLIDO = $request->NOMBRE_APELLIDO;
        $atencion->ASEGURADORA = $request->ASEGURADORA;
        $atencion->TIPO_SEGURO = $request->TIPO_SEGURO;
        $atencion->TERAPIAS = $request->TERAPIAS;
        $atencion->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $atencion = Atencion_Diaria::find($request->id);
        $atencion->FECHA = $request->FECHA;
        $atencion->TERAPEUTA = $request->TERAPEUTA;
        $atencion->NOMBRE_APELLIDO = $request->NOMBRE_APELLIDO;
        $atencion->ASEGURADORA = $request->ASEGURADORA;
        $atencion->TIPO_SEGURO = $request->TIPO_SEGURO;
        $atencion->TERAPIAS = $request->TERAPIAS;
        $atencion->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $atencion = Atencion_Diaria::find($request->id);
        $atencion->delete();
        return response()->json(['data' => 'ok']);
    }
}
