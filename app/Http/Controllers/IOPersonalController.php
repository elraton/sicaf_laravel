<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\I_O_Personal;

class IOPersonalController extends Controller
{
    public function index() {
        $IOPersonal = I_O_Personal::all();

        return $IOPersonal->toJson();
    }

    public function store(Request $request) {
        $status = $request->ESTADO;
        if ( $request->TIPO_REGISTRO == 'INGRESO' ) {
            $IOPersonal = I_O_Personal::where('FECHA', '=', $request->FECHA)
            ->where('CODIGO', '=', $request->CODIGO)
            ->where('TIPO_REGISTRO', '=', 'INGRESO')
            ->get();

            if (sizeof($IOPersonal) > 0) {
                $status = 'REINGRESO';
            }
        }
        
        $IOPersonal = new I_O_Personal;
        $IOPersonal->FECHA = $request->FECHA;
        $IOPersonal->CODIGO = $request->CODIGO;
        $IOPersonal->NOMBRE_APELLIDO = $request->NOMBRE_APELLIDO;
        $IOPersonal->TIPO_REGISTRO = $request->TIPO_REGISTRO;
        $IOPersonal->TURNO = $request->TURNO;
        $IOPersonal->SEDE = $request->SEDE;
        $IOPersonal->HORA_PERSONAL = $request->HORA_PERSONAL;
        $IOPersonal->HORA_REGISTRO = $request->HORA_REGISTRO;
        $IOPersonal->ESTADO = $status;
        $IOPersonal->ATENCIONES = $request->ATENCIONES;
        $IOPersonal->save();
        return response()->json(['data' => 'ok']);
    }

    public function byDay(Request $request) {
        $IOPersonal = I_O_Personal::where('FECHA', '=', $request->FECHA)
        ->where('SEDE', '=', $request->SEDE)
        ->get();
        return $IOPersonal->toJson();
    }

    public function byTurn(Request $request) {
        $IOPersonal = I_O_Personal::where('FECHA', '=', $request->FECHA)
        ->where('SEDE', '=', $request->SEDE)
        ->where('TURNO', '=', $request->TURNO)
        ->get();
        return $IOPersonal->toJson();
    }
}
