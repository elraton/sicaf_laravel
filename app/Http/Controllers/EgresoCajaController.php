<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Egresos_Caja;

class EgresoCajaController extends Controller
{
    public function index() {
        $egresos = Egresos_Caja::all();

        return $egresos->toJson();
    }

    public function store(Request $request) {
        $egreso = new Egresos_Caja;
        $egreso->FECHA = $request->FECHA;
        $egreso->SEDE = $request->SEDE;
        $egreso->EMISOR = $request->EMISOR;
        $egreso->RECEPTOR = $request->RECEPTOR;
        $egreso->IMPORTE = $request->IMPORTE;
        $egreso->TURNO = $request->TURNO;
        $egreso->DESCRIPCION = $request->DESCRIPCION;
        $egreso->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $egreso = Egresos_Caja::find($request->id);
        $egreso->FECHA = $request->FECHA;
        $egreso->SEDE = $request->SEDE;
        $egreso->EMISOR = $request->EMISOR;
        $egreso->RECEPTOR = $request->RECEPTOR;
        $egreso->IMPORTE = $request->IMPORTE;
        $egreso->TURNO = $request->TURNO;
        $egreso->DESCRIPCION = $request->DESCRIPCION;
        $egreso->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $egreso = Egresos_Caja::find($request->id);
        $egreso->delete();
        return response()->json(['data' => 'ok']);
    }
}
