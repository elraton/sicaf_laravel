<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ventas_Caja;

class VentasCajaController extends Controller
{
    public function index() {
        $ventas = Ventas_Caja::all();

        return $ventas->toJson();
    }

    public function store(Request $request) {
        $venta = new Ventas_Caja;
        $venta->FECHA = $request->FECHA;
        $venta->ITEM = $request->ITEM;
        $venta->MARCA = $request->MARCA;
        $venta->TIPO = $request->TIPO;
        $venta->MODELO = $request->MODELO;
        $venta->CANTIDAD = $request->CANTIDAD;
        $venta->PRECIO_UNIDAD = $request->PRECIO_UNIDAD;
        $venta->SUBTOTAL = $request->SUBTOTAL;
        $venta->IGV = $request->IGV;
        $venta->TOTAL = $request->TOTAL;
        $venta->PERSONAL_FISIOVIDA = $request->PERSONAL_FISIOVIDA;
        $venta->CODIGO_VENTA = $request->CODIGO_VENTA;
        $venta->SEDE = $request->SEDE;
        $venta->USUARIO = $request->USUARIO;
        $venta->TURNO = $request->TURNO;
        $venta->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $venta = Ventas_Caja::find($request->id);
        $venta->FECHA = $request->FECHA;
        $venta->ITEM = $request->ITEM;
        $venta->MARCA = $request->MARCA;
        $venta->TIPO = $request->TIPO;
        $venta->MODELO = $request->MODELO;
        $venta->CANTIDAD = $request->CANTIDAD;
        $venta->PRECIO_UNIDAD = $request->PRECIO_UNIDAD;
        $venta->SUBTOTAL = $request->SUBTOTAL;
        $venta->IGV = $request->IGV;
        $venta->TOTAL = $request->TOTAL;
        $venta->PERSONAL_FISIOVIDA = $request->PERSONAL_FISIOVIDA;
        $venta->CODIGO_VENTA = $request->CODIGO_VENTA;
        $venta->SEDE = $request->SEDE;
        $venta->USUARIO = $request->USUARIO;
        $venta->TURNO = $request->TURNO;
        $venta->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $venta = Ventas_Caja::find($request->id);
        $venta->delete();
        return response()->json(['data' => 'ok']);
    }
}
