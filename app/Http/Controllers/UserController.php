<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index($id) {
        $user = User::find($id);

        return $user->toJson();
    }

    public function store(Request $request) {
        $user = new User;
        $user->NOMBRE = $request->NOMBRE;
        $user->APELLIDO = $request->APELLIDO;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->AREA = $request->AREA;
        $user->photo = $request->photo;
        $user->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $user = User::find($request->id);
        $user->NOMBRE = $request->NOMBRE;
        $user->APELLIDO = $request->APELLIDO;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->AREA = $request->AREA;
        $user->photo = $request->photo;
        $user->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $user = User::find($request->id);
        $user->delete();
        return response()->json(['data' => 'ok']);
    }
}
