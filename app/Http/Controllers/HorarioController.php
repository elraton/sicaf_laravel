<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio_Paciente;
use App\horarios;

class HorarioController extends Controller
{
    public function index() {
        $horarios = horarios::all();

        return $horarios->toJson();
    }

    public function getbyrange(Request $request) {
        $horarios = horarios::where('horario', '>', $request->from)
                            ->where('horario', '<', $request->to)->with('treatment')->get();

        return $horarios->toJson();
    }

    public function saveOne(Request $request) {
        $servicio = Servicio_Paciente::find($request->treatment);
        $horario = new horarios;
        $horario->horario = $request->horario;
        $horario->session = $request->session;
        $horario->status = $request->status;
        $horario->personal_id = $request->personal_id;
        $horario->save();

        $servicio->schedule()->attach($horario);
        return response()->json(['data' => 'ok']);
    }

    public function store(Request $request) {
        $horario = new horarios;
        $horario->horario = $request->horario;
        $horario->session = $request->session;
        $horario->status = $request->status;
        $horario->personal_id = $request->personal_id;
        $horario->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $horario = horarios::find($request->id);
        $horario->horario = $request->horario;
        $horario->session = $request->session;
        $horario->status = $request->status;
        $horario->personal_id = $request->personal_id;
        $horario->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $horario = horarios::find($request->id);
        $horario->delete();
        return response()->json(['data' => 'ok']);
    }
}
