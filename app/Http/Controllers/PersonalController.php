<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Personal;
use App\Per_Category;
use App\Per_Horario;

class PersonalController extends Controller
{
    public function index() {
        $personal = Personal::with('horarios')->get();
        return $personal->toJson();
    }

    public function show(Request $request) {
        $personal = Personal::find($request->ID)->get();

        return $personal->toJson();
    }

    public function store(Request $request) {
        $personal = new Personal;
        
        $personal->DNI = $request->DNI;
        $personal->NOMBRE = $request->NOMBRE;
        $personal->NOMBRE2 = $request->NOMBRE2;
        $personal->APELLIDO = $request->APELLIDO;
        $personal->APELLIDO2 = $request->APELLIDO2;
        $personal->CELULAR = $request->CELULAR;
        $personal->TELEFONO = $request->TELEFONO;
        $personal->SEDE = $request->SEDE;
        $personal->TURNO = $request->TURNO;
        $personal->AREA = $request->AREA;
        $personal->FECHA_INGRESO = $request->FECHA_INGRESO;
        $personal->FECHA_NACIMIENTO = $request->FECHA_NACIMIENTO;
        $personal->DEPARTAMENTO = $request->DEPARTAMENTO;
        $personal->CIUDAD = $request->CIUDAD;
        $personal->DIRECCION = $request->DIRECCION;
        $personal->DISTRITO = $request->DISTRITO;
        $personal->ESTADO = $request->ESTADO;
        $personal->CONDICION = $request->CONDICION;
        $personal->EMAIL = $request->EMAIL;
        $personal->ULTIMO_TRABAJO = $request->ULTIMO_TRABAJO;
        $personal->ULTIMA_POSICION = $request->ULTIMA_POSICION;
        $personal->ESTADO_CIVIL = $request->ESTADO_CIVIL;
        $personal->NMRO_CUENTA = $request->NMRO_CUENTA;
        $personal->ES_DOCTOR = $request->ES_DOCTOR;
        $personal->ES_NIÑOS = $request->ES_NIÑOS;
        $personal->CATEGORIA = $request->CATEGORIA;
        $personal->PASSWORD = $request->PASSWORD;
        $personal->save();
        foreach ($request->horarios as $horario) {
            // $data = Per_Horario::find($horario);
            $personal->horarios()->attach($horario);
        }
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $personal = Personal::find($request->id);
        $personal->DNI = $request->DNI;
        $personal->NOMBRE = $request->NOMBRE;
        $personal->NOMBRE2 = $request->NOMBRE2;
        $personal->APELLIDO = $request->APELLIDO;
        $personal->APELLIDO2 = $request->APELLIDO2;
        $personal->CELULAR = $request->CELULAR;
        $personal->TELEFONO = $request->TELEFONO;
        $personal->SEDE = $request->SEDE;
        $personal->TURNO = $request->TURNO;
        $personal->AREA = $request->AREA;
        $personal->FECHA_INGRESO = $request->FECHA_INGRESO;
        $personal->FECHA_NACIMIENTO = $request->FECHA_NACIMIENTO;
        $personal->DEPARTAMENTO = $request->DEPARTAMENTO;
        $personal->CIUDAD = $request->CIUDAD;
        $personal->DIRECCION = $request->DIRECCION;
        $personal->DISTRITO = $request->DISTRITO;
        $personal->ESTADO = $request->ESTADO;
        $personal->CONDICION = $request->CONDICION;
        $personal->EMAIL = $request->EMAIL;
        $personal->ULTIMO_TRABAJO = $request->ULTIMO_TRABAJO;
        $personal->ULTIMA_POSICION = $request->ULTIMA_POSICION;
        $personal->ESTADO_CIVIL = $request->ESTADO_CIVIL;
        $personal->NMRO_CUENTA = $request->NMRO_CUENTA;
        $personal->ES_DOCTOR = $request->ES_DOCTOR;
        $personal->ES_NIÑOS = $request->ES_NIÑOS;
        $personal->CATEGORIA = $request->CATEGORIA;
        foreach ($request->horarios as $horario) {
            // $data = Per_Horario::find($horario);
            $personal->horarios()->attach($horario);
        }
        $personal->save();
        return response()->json(['data' => $personal ]);
    }

    public function delete(Request $request) {
        $personal = Personal::find($request->id);
        $personal->delete();
        return response()->json(['data' => 'ok']);
    }
}
