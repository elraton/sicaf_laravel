<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Per_Horario;

class PersonalScheduleController extends Controller
{
    public function index() {
        $horarios = Per_Horario::all();

        return $horarios->toJson();
    }

    public function store(Request $request) {
        $horario = new Per_Horario;
        $horario->DIA = $request->DIA;
        $horario->HORA_INICIO = $request->HORA_INICIO;
        $horario->HORA_FIN = $request->HORA_FIN;
        $horario->NOMBRE = $request->NOMBRE;
        $horario->TIEMPO_ATENCION = $request->TIEMPO_ATENCION;
        $horario->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $horario = Per_Horario::find($request->id);
        $horario->DIA = $request->DIA;
        $horario->HORA_INICIO = $request->HORA_INICIO;
        $horario->HORA_FIN = $request->HORA_FIN;
        $horario->NOMBRE = $request->NOMBRE;
        $horario->TIEMPO_ATENCION = $request->TIEMPO_ATENCION;
        $horario->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $horario = Per_Horario::find($request->id);
        $horario->delete();
        return response()->json(['data' => 'ok']);
    }    
}
