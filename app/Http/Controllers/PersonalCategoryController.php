<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Per_Category;

class PersonalCategoryController extends Controller
{
    public function index() {
        $categories = Per_Category::all();

        return $categories->toJson();
    }

    public function store(Request $request) {
        $category = new Per_Category;
        $category->NOMBRE = $request->NOMBRE;
        $category->SALARIO = $request->SALARIO;
        $category->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $category = Per_Category::find($request->id);
        $category->NOMBRE = $request->NOMBRE;
        $category->SALARIO = $request->SALARIO;
        $category->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $category = Per_Category::find($request->id);
        $category->delete();
        return response()->json(['data' => 'ok']);
    }
}
