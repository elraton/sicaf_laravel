<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tarifas_Servicios;

class TarifasServiciosController extends Controller
{
    public function index() {
        $tarifas = Tarifas_Servicios::all();

        return $tarifas->toJson();
    }

    public function store(Request $request) {
        $tarifa = new Tarifas_Servicios;
        $tarifa->CODIGO = $request->CODIGO;
        $tarifa->SERVICIO = $request->SERVICIO;
        $tarifa->TIPO_TARIFA = $request->TIPO_TARIFA;
        $tarifa->TIPO_PACIENTE = $request->TIPO_PACIENTE;
        $tarifa->PRECIO = $request->PRECIO;
        $tarifa->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $tarifa = Tarifas_Servicios::find($request->id);
        $tarifa->CODIGO = $request->CODIGO;
        $tarifa->SERVICIO = $request->SERVICIO;
        $tarifa->TIPO_TARIFA = $request->TIPO_TARIFA;
        $tarifa->TIPO_PACIENTE = $request->TIPO_PACIENTE;
        $tarifa->PRECIO = $request->PRECIO;
        $tarifa->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $tarifa = Tarifas_Servicios::find($request->id);
        $tarifa->delete();
        return response()->json(['data' => 'ok']);
    }
}
