<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Codigos_Promociones;

class CodigosPromosController extends Controller
{
    public function index() {
        $codigos = Codigos_Promociones::all();

        return $codigos->toJson();
    }

    public function store(Request $request) {
        $codigo = new Codigos_Promociones;
        $codigo->CODIGO = $request->CODIGO;
        $codigo->TIPO = $request->TIPO;
        $codigo->ESTADO = $request->ESTADO;
        $codigo->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $codigo = Codigos_Promociones::find($request->id);
        $codigo->CODIGO = $request->CODIGO;
        $codigo->TIPO = $request->TIPO;
        $codigo->ESTADO = $request->ESTADO;
        $codigo->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $codigo = Codigos_Promociones::find($request->id);
        $codigo->delete();
        return response()->json(['data' => 'ok']);
    }
}
