<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;

class PacientesController extends Controller
{
    public function index() {
        $pacientes = Paciente::all();

        return $pacientes->toJson();
    }

    public function searchbyDNI(Request $request) {
        $paciente = Paciente::where('DNI', '=', $request->DNI)->get();

        return $paciente->toJson();
    }

    public function store(Request $request) {
        $paciente = new Paciente;
        
        $last_pacient = Paciente::orderBy('HHCC', 'desc')->first();
        $HHCC = 1;
        if($last_pacient) {
            $HHCC = $last_pacient->HHCC + 1;
        }
        $paciente->SEDE = $request->SEDE;
        $paciente->DNI = $request->DNI;
        $paciente->P_NOMBRE = $request->P_NOMBRE;
        $paciente->S_NOMBRE = $request->S_NOMBRE;
        $paciente->P_APELLIDO = $request->P_APELLIDO;
        $paciente->S_APELLIDO = $request->S_APELLIDO;
        $paciente->CELULAR = $request->CELULAR;
        $paciente->TELEFONO = $request->TELEFONO;
        $paciente->DIRECCION = $request->DIRECCION;
        $paciente->DISTRITO = $request->DISTRITO;
        $paciente->F_NACIMIENTO = $request->F_NACIMIENTO;
        $paciente->NOMBRE_TUTOR = $request->NOMBRE_TUTOR;
        $paciente->CELULAR_TUTOR = $request->CELULAR_TUTOR;
        $paciente->EMAIL = $request->EMAIL;
        $paciente->TIPO_PACIENTE = $request->TIPO_PACIENTE;
        $paciente->ASEGURADORA = $request->ASEGURADORA;
        $paciente->TIPO_SEGURO = $request->TIPO_SEGURO;
        $paciente->COPAGO_FIJO = $request->COPAGO_FIJO;
        $paciente->COPAGO_VARIABLE = $request->COPAGO_VARIABLE;
        $paciente->F_REGISTRO = $request->F_REGISTRO;
        $paciente->HHCC = $HHCC;
        $paciente->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $paciente = Paciente::find($request->id);
        $paciente->HHCC = $request->HHCC;
        $paciente->SEDE = $request->SEDE;
        $paciente->DNI = $request->DNI;
        $paciente->P_NOMBRE = $request->P_NOMBRE;
        $paciente->S_NOMBRE = $request->S_NOMBRE;
        $paciente->P_APELLIDO = $request->P_APELLIDO;
        $paciente->S_APELLIDO = $request->S_APELLIDO;
        $paciente->CELULAR = $request->CELULAR;
        $paciente->TELEFONO = $request->TELEFONO;
        $paciente->DIRECCION = $request->DIRECCION;
        $paciente->DISTRITO = $request->DISTRITO;
        $paciente->F_NACIMIENTO = $request->F_NACIMIENTO;
        $paciente->NOMBRE_TUTOR = $request->NOMBRE_TUTOR;
        $paciente->CELULAR_TUTOR = $request->CELULAR_TUTOR;
        $paciente->EMAIL = $request->EMAIL;
        $paciente->TIPO_PACIENTE = $request->TIPO_PACIENTE;
        $paciente->ASEGURADORA = $request->ASEGURADORA;
        $paciente->TIPO_SEGURO = $request->TIPO_SEGURO;
        $paciente->COPAGO_FIJO = $request->COPAGO_FIJO;
        $paciente->COPAGO_VARIABLE = $request->COPAGO_VARIABLE;
        $paciente->F_REGISTRO = $request->F_REGISTRO;
        $paciente->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $paciente = Paciente::find($request->id);
        $paciente->delete();
        return response()->json(['data' => 'ok']);
    }
}
