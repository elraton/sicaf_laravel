<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio_Paciente;
use App\horarios;

class ServicioPacienteController extends Controller
{
    public function index() {
        $servicios = Servicio_Paciente::with('schedule')->get();

        return $servicios->toJson();
    }
    
    public function getbyRange(Request $request) {
        $servicios = Servicio_Paciente::where('FECHA', '>', $request->from)
        ->where('FECHA', '<', $request->to)->where('DOCTOR', '!=', null)->get();


        return $servicios->toJson();
    }

    public function getOne(Request $request) {
        $servicio = Servicio_Paciente::where('id', '=', $request->id)->with('schedule')->first();

        return $servicio->toJson();
    }

    public function store(Request $request) {
        $servicio = new Servicio_Paciente;
        $servicio->DNI = $request->DNI;
        $servicio->P_NOMBRE = $request->P_NOMBRE;
        $servicio->P_APELLIDO = $request->P_APELLIDO;
        $servicio->TERAPEUTA = $request->TERAPEUTA;
        $servicio->TERAPEUTA_ID = $request->TERAPEUTA_ID;
        $servicio->DOCTOR = $request->DOCTOR;
        $servicio->DOCTOR_ID = $request->DOCTOR_ID;
        $servicio->ESTADO = $request->ESTADO;
        $servicio->INTERVALO = $request->INTERVALO;
        $servicio->FECHA = $request->FECHA;
        $servicio->HORARIO = $request->HORARIO;
        $servicio->HORARIO2 = $request->HORARIO2;
        $servicio->S_PROGRAMADAS = $request->S_PROGRAMADAS;
        $servicio->S_REALIZADAS = $request->S_REALIZADAS;
        $servicio->S_PAGADAS = $request->S_PAGADAS;
        $servicio->CONSULTA_GRATIS = $request->CONSULTA_GRATIS;
        $servicio->SERVICIO = $request->SERVICIO;
        $servicio->ABONO_TERAPIA = $request->ABONO_TERAPIA;
        $servicio->T_PROMOCION = $request->T_PROMOCION;
        $servicio->save();
        foreach ($request->HORARIOS as $horario) {
            $newHorario = new horarios();
            $newHorario->horario = $horario['horario'];
            $newHorario->session = $horario['session'];
            $newHorario->personal_id = $horario['personal_id'];
            $newHorario->save();
            $servicio->schedule()->attach($newHorario);
        }
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $servicio = Servicio_Paciente::find($request->id);
        $servicio->DNI = $request->DNI;
        $servicio->P_NOMBRE = $request->P_NOMBRE;
        $servicio->P_APELLIDO = $request->P_APELLIDO;
        $servicio->TERAPEUTA = $request->TERAPEUTA;
        $servicio->TERAPEUTA_ID = $request->TERAPEUTA_ID;
        $servicio->DOCTOR = $request->DOCTOR;
        $servicio->DOCTOR_ID = $request->DOCTOR_ID;
        $servicio->ESTADO = $request->ESTADO;
        $servicio->INTERVALO = $request->INTERVALO;
        $servicio->FECHA = $request->FECHA;
        $servicio->HORARIO = $request->HORARIO;
        $servicio->HORARIO2 = $request->HORARIO2;
        $servicio->S_PROGRAMADAS = $request->S_PROGRAMADAS;
        $servicio->S_REALIZADAS = $request->S_REALIZADAS;
        $servicio->S_PAGADAS = $request->S_PAGADAS;
        $servicio->CONSULTA_GRATIS = $request->CONSULTA_GRATIS;
        $servicio->SERVICIO = $request->SERVICIO;
        $servicio->ABONO_TERAPIA = $request->ABONO_TERAPIA;
        $servicio->T_PROMOCION = $request->T_PROMOCION;
        $servicio->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $servicio = Servicio_Paciente::find($request->id);
        foreach ($servicio->schedule()->get() as $element) {
            $horario = horarios::find($element->id);
            $horario->delete();
        }
        $servicio->schedule()->detach();
        $servicio->delete();
        return response()->json(['data' => 'ok']);
    }
}
