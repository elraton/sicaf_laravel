<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingresos_Caja;

class IngresoCajaController extends Controller
{
    public function index() {
        $ingresos = Ingresos_Caja::all();

        return $ingresos->toJson();
    }

    public function store(Request $request) {
        $ingreso = new Ingresos_Caja;
        $ingreso->FECHA = $request->FECHA;
        $ingreso->DNI = $request->DNI;
        $ingreso->NOMBRE_APELLIDO = $request->NOMBRE_APELLIDO;
        $ingreso->TIPO_PACIENTE = $request->TIPO_PACIENTE;
        $ingreso->TIPO_SERVICIO = $request->TIPO_SERVICIO;
        $ingreso->PRECIO_UNIDAD = $request->PRECIO_UNIDAD;
        $ingreso->CANTIDAD = $request->CANTIDAD;
        $ingreso->SUBTOTAL = $request->SUBTOTAL;
        $ingreso->IGV = $request->IGV;
        $ingreso->TOTAL = $request->TOTAL;
        $ingreso->TOTAL_DESCUENTO = $request->TOTAL_DESCUENTO;
        $ingreso->DESCUENTO = $request->DESCUENTO;
        $ingreso->TOTAL_PAGADO = $request->TOTAL_PAGADO;
        $ingreso->PROMO_MENSUAL = $request->PROMO_MENSUAL;
        $ingreso->COD_PROMOCION = $request->COD_PROMOCION;
        $ingreso->TIPO_CODIGO = $request->TIPO_CODIGO;
        $ingreso->FORMA_PAGO = $request->FORMA_PAGO;
        $ingreso->TIPO_COMPROBANTE = $request->TIPO_COMPROBANTE;
        $ingreso->RECEPTOR = $request->RECEPTOR;
        $ingreso->TURNO = $request->TURNO;
        $ingreso->SEDE = $request->SEDE;
        $ingreso->save();
        return response()->json(['data' => 'ok']);
    }

    public function update(Request $request) {
        $ingreso = Ingresos_Caja::find($request->id);
        $ingreso->FECHA = $request->FECHA;
        $ingreso->DNI = $request->DNI;
        $ingreso->NOMBRE_APELLIDO = $request->NOMBRE_APELLIDO;
        $ingreso->TIPO_PACIENTE = $request->TIPO_PACIENTE;
        $ingreso->TIPO_SERVICIO = $request->TIPO_SERVICIO;
        $ingreso->PRECIO_UNIDAD = $request->PRECIO_UNIDAD;
        $ingreso->CANTIDAD = $request->CANTIDAD;
        $ingreso->SUBTOTAL = $request->SUBTOTAL;
        $ingreso->IGV = $request->IGV;
        $ingreso->TOTAL = $request->TOTAL;
        $ingreso->TOTAL_DESCUENTO = $request->TOTAL_DESCUENTO;
        $ingreso->DESCUENTO = $request->DESCUENTO;
        $ingreso->TOTAL_PAGADO = $request->TOTAL_PAGADO;
        $ingreso->PROMO_MENSUAL = $request->PROMO_MENSUAL;
        $ingreso->COD_PROMOCION = $request->COD_PROMOCION;
        $ingreso->TIPO_CODIGO = $request->TIPO_CODIGO;
        $ingreso->FORMA_PAGO = $request->FORMA_PAGO;
        $ingreso->TIPO_COMPROBANTE = $request->TIPO_COMPROBANTE;
        $ingreso->RECEPTOR = $request->RECEPTOR;
        $ingreso->TURNO = $request->TURNO;
        $ingreso->SEDE = $request->SEDE;
        $ingreso->save();
        return response()->json(['data' => 'ok']);
    }

    public function delete(Request $request) {
        $ingreso = Ingresos_Caja::find($request->id);
        $ingreso->delete();
        return response()->json(['data' => 'ok']);
    }
}
