<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    public function horarios() {
        return $this->belongsToMany('App\Per_Horario');
    }
}
